import os
#import matplotlib.pyplot as plt
from core.model import Model
import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
import socket
import ast
import json

def normalise_windows(window_data, single_window=False):
    '''Normalise window with a base value of zero'''
    normalised_data = []
    window_data = [window_data] if single_window else window_data
    for window in window_data:
        normalised_window = []
        for col_i in range(window.shape[1]):
            normalised_col = [((float(p) / float(window[0, col_i])) - 1) for p in window[:, col_i]]
            normalised_window.append(normalised_col)
        normalised_window = np.array(normalised_window).T # reshape and transpose array back into original multidimensional format
        normalised_data.append(normalised_window)
    return np.array(normalised_data)

def denormalise_windows(data_test, predict, time):
    '''Normalise window with a base value of zero'''
    denormalise_data = []
    value1 = []
    value1.append(str(time).replace('-', '.'))
    value1.append(str(float(data_test[-1, [0]])))
    denormalise_data.append(value1)
    for i in range(len(predict)):
        value = []
        newtime = time + timedelta(minutes=15*(i+1))
        # print(newtime)

        value.append(str(newtime).replace('-','.'))
        print()
        value.append(str((predict[i]+1)*float(data_test[i+48, [0]])))
        denormalise_data.append(value)

    return np.array(denormalise_data)


def train_test_model(msg=''):
    msg = msg.replace('true', 'True')
    msg = msg.replace('false', 'False')
    msg = ast.literal_eval(msg)

    if (type(msg) == dict):
        input_data = msg
    else:
        return "BAD JSON!!"
    open = input_data['Open']
    high = input_data['High']
    close = input_data['Close']
    low = input_data['Low']
    volume = input_data['Volume']
    datereal = input_data['Date']
    filename = input_data['FileName']
    bars = input_data['Bars']
    print("py1: Load data input ok")
    # configs = json.load(open('config.json', 'r'))
    # if not os.path.exists(configs['model']['save_dir']): os.makedirs(configs['model']['save_dir'])
    #dataframe = pd.read_csv(filename)
    #print(volume)
    dataframe = pd.DataFrame({'Date': datereal,'Open': open, 'High': high,'Close': close,'Low': low,'Volume': volume}, columns=['Date','Open', 'High', 'Close', 'Low', 'Volume'])

    #print(dataframe)

    number_point_predic = int(bars)

    data_test = dataframe.get(["Close","Volume"],).values[len(dataframe)-49-number_point_predic-20:len(dataframe)-20]
    data_datetime = dataframe.get(["Date"],).values[len(dataframe)-49-number_point_predic-20:len(dataframe)-20]

    #print(data_test)
    time_date_end = str(data_datetime[-1])
    print(time_date_end)

    Y = int(time_date_end.split(".")[0].split("['")[1])
    M = int(time_date_end.split(".")[1])
    D = int(time_date_end.split(".")[2].split(" ")[0])
    h = int(time_date_end.split(" ")[1].split(":")[0])
    m = int(time_date_end.split(" ")[1].split(":")[1])

    time_end = datetime(Y, M, D, h, m)
    # print(time_end)
    # B = time_end + timedelta(minutes=15)
    # print(B)
    # print(len(data_datetime))
    # print(len(data_test))

    # input()
    data_windows = []
    for i in range(len(data_test) - 49):
        data_windows.append(data_test[i:i + 49])
    normalise = True
    data_windows = np.array(data_windows).astype(float)
    #print(data_windows)
    data_windows = normalise_windows(data_windows, single_window=False) if normalise else data_windows
    # print(data_windows)
    x = data_windows[:, :-1]
    #print('xxxxx',x.shape)
    y = data_windows[:, -1, [0]]
    # print('yyyy', y.shape)

    x_test = data_windows[:, :]
    # print(x_test.shape)

    # print(len(data_windows))
    # print(len(x_test))
    print("py2: Xu ly data ok")
    model = Model()
    model.load_model('saved_models/USDJPY_15012020-134934-15M.h5')
    print("py3: Load model ok")
    predictions = model.predict_point_by_point(x_test)
    print(x_test)

    print(predictions)
    pre_real = denormalise_windows(data_test, predictions, time_end)

    print(pre_real)
    # input()
    # plt.plot(predictions, label='Prediction')
    # print('XXXXXXXXXX',pre_real)
    # print(str(pre_real[:,0]))
    # input()
    #plt.plot(pre_real[:,1], label='Prediction_real')
    #plt.legend()
    #plt.show()
    # # plot_results_multiple(predictions, y_test, configs['data']['sequence_length'])
    # # plot_results(predictions, y_test)
    print("py4: Predict ok")
    responseJSON = {}
    responseJSON['PredDate'] = pre_real[:,0].tolist()
    responseJSON['PredClose'] = pre_real[:,1].tolist()
    responseJSON['LengPre'] = len(pre_real[:,0])
    return json.dumps(responseJSON) + '\r\n'

def main():
    ""


class socketserver:
    def __init__(self, address='', port=9090):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.address = address
        self.port = port
        self.sock.bind((self.address, self.port))
        self.cummdata = ''

    def recvmsg(self):
        self.sock.listen(1)
        self.conn, self.addr = self.sock.accept()
        print('connected to', self.addr)
        self.cummdata = ''

        while True:
            data = self.conn.recv(10000000)
            self.cummdata += data.decode("utf-8")
            if not data:
                break
            self.conn.send(bytes(train_test_model(self.cummdata), "utf-8"))
            return self.cummdata

    def __del__(self):
        self.sock.close()


serv = socketserver('127.0.0.1', 9090)

print('Socket Created at {}. Waiting for client..'.format(serv.sock.getsockname()))

while True:
    msg = serv.recvmsg()
